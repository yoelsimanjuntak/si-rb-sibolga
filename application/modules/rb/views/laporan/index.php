<?php
$ruser = GetLoggedUser();
$rOptRenja = array();
if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $this->db->where(TBL_RB_RENJA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
}

$rOptRenja = $this->db
->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
->order_by(COL_NMTYPE, 'asc')
->order_by(COL_TAHUN, 'desc')
->get(TBL_RB_RENJA)
->result_array();

$rrenja = array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <form method="get" action="<?=current_url()?>">
            <div class="card-body">
              <div class="form-group row">
                <label class="control-label col-lg-2">DOK. RENJA</label>
                <div class="col-sm-10">
                  <select class="form-control" name="IdRenja">
                    <?php
                    foreach($rOptRenja as $opt) {
                      $isSelected = '';
                      if(!empty($_GET['IdRenja']) && $_GET['IdRenja']==$opt[COL_UNIQ]) {
                        $isSelected='selected';
                      }
                      ?>
                      <option value="<?=$opt[COL_UNIQ]?>" <?=$isSelected?>>
                        <?=$opt[COL_TAHUN].' - '.(!empty($opt[COL_SKPDNAMA])?strtoupper($opt[COL_SKPDNAMA]).(!empty($opt[COL_NMKETERANGAN])?' : '.$opt[COL_NMKETERANGAN]:''):$opt[COL_NMKETERANGAN])?>
                      </option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <?php
              if($tipe=='bulan') {
                ?>
                <div class="form-group row">
                  <label class="control-label col-lg-2">PERIODE</label>
                  <div class="col-sm-3">
                    <select class="form-control" name="Period">
                      <option value="1" <?=!empty($_GET['Period'])&&$_GET['Period']=='1'?'selected':''?>>JANUARI</option>
                      <option value="2" <?=!empty($_GET['Period'])&&$_GET['Period']=='2'?'selected':''?>>FEBRUARI</option>
                      <option value="3" <?=!empty($_GET['Period'])&&$_GET['Period']=='3'?'selected':''?>>MARET</option>
                      <option value="4" <?=!empty($_GET['Period'])&&$_GET['Period']=='4'?'selected':''?>>APRIL</option>
                      <option value="5" <?=!empty($_GET['Period'])&&$_GET['Period']=='5'?'selected':''?>>MEI</option>
                      <option value="6" <?=!empty($_GET['Period'])&&$_GET['Period']=='6'?'selected':''?>>JUNI</option>
                      <option value="7" <?=!empty($_GET['Period'])&&$_GET['Period']=='7'?'selected':''?>>JULI</option>
                      <option value="8" <?=!empty($_GET['Period'])&&$_GET['Period']=='8'?'selected':''?>>AGUSTUS</option>
                      <option value="9" <?=!empty($_GET['Period'])&&$_GET['Period']=='9'?'selected':''?>>SEPTEMBER</option>
                      <option value="10" <?=!empty($_GET['Period'])&&$_GET['Period']=='10'?'selected':''?>>OKTOBER</option>
                      <option value="11" <?=!empty($_GET['Period'])&&$_GET['Period']=='11'?'selected':''?>>NOVEMBER</option>
                      <option value="12" <?=!empty($_GET['Period'])&&$_GET['Period']=='12'?'selected':''?>>DESEMBER</option>
                    </select>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-chevron-circle-right"></i>&nbsp;LIHAT</button>
            </div>
          </form>
        </div>
        <?php
        if(!empty($_GET['IdRenja'])) {
          $rrenja = $this->db->where(COL_UNIQ, $_GET['IdRenja'])->get(TBL_RB_RENJA)->row_array();
          if(!empty($rrenja)) {
            ?>
            <div class="card card-default">
              <div class="card-header">
                <h6 class="font-weight-bold text-center">
                  LAPORAN MONITORING DAN EVALUASI<br />
                  REFORMASI BIROKRASI<br />
                  PERIODE <?=$tipe=='bulan'?'BULAN '.str_pad($_GET['Period'],2,'0', STR_PAD_LEFT).' TAHUN '.$rrenja[COL_TAHUN]:'TAHUN '.$rrenja[COL_TAHUN]?>
                </h6>
              </div>
              <div class="card-body p-0">
                <?=$this->load->view('rb/laporan/index-partial', array('rrenja'=>$rrenja, 'tipe'=>$tipe), true)?>
              </div>
            </div>
            <?php
          }
        }
        ?>

      </div>
    </div>
  </div>
</section>
