<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      $d[COL_KD_URUSAN].".".$d[COL_KD_BIDANG].".".$d[COL_KD_UNIT].".".$d[COL_KD_SUB],
      anchor('site/home/status-opd?Kd_Urusan='.$d[COL_KD_URUSAN].'&Kd_Bidang='.$d[COL_KD_BIDANG].'&Kd_Unit='.$d[COL_KD_UNIT].'&Kd_Sub='.$d[COL_KD_SUB],$d[COL_NM_SUB_UNIT]),
      number_format($d["Tujuan"], 0),
      number_format($d["Sasaran"], 0),
      number_format($d["Program"], 0),
      number_format($d["Kegiatan"], 0),
      number_format($d["Cascading_Es3"], 0),
      number_format($d["Cascading_Es4"], 0),
      number_format($d["DPA_Prg"], 0),
      number_format($d["DPA_Keg"], 0)
    );
    $i++;
}
$data = json_encode($res);
 ?>
<div class="content-header">
    <!--<div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>
    </div>-->
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header with-border">
                    <h4 class="card-title">Status OPD</h4>
                </div>
                <div class="card-body">
                  <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered">
                      <thead>
                        <tr>
                          <th class="border-top-0 text-center" rowspan="2">Kode</th>
                          <th class="border-top-0 text-center" rowspan="2">OPD</th>
                          <th class="border-top-0 text-center" colspan="2">Renstra</th>
                          <th class="border-top-0 text-center" colspan="2">Renja</th>
                          <th class="border-top-0 text-center" colspan="2">Cascading</th>
                          <th class="border-top-0 text-center" colspan="2">DPA</th>
                        </tr>
                        <tr>
                          <th class="border-top-0 text-center">Tujuan</th>
                          <th class="border-top-0 text-center">Sasaran</th>
                          <th class="border-top-0 text-center">Program</th>
                          <th class="border-top-0 text-center">Kegiatan</th>
                          <th class="border-top-0 text-center">Es. III</th>
                          <th class="border-top-0 text-center">Es. IV</th>
                          <th class="border-top-0 text-center">Program</th>
                          <th class="border-top-0 text-center">Kegiatan</th>
                        </tr>
                      </thead>
                    </table>
                  </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
          "autoWidth" : false,
          "aaData": <?=$data?>,
          "scrollX": "120%",
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
          "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          //"dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
          "ordering": false,
          "columnDefs":[
            {targets:[2,3,4,5,6,7,8,9],className:'text-right'}
          ],
          "rowCallback": function( row, data, index ) {
            console.log(data);
            for(var i=0; i<data.length; i++) {
              if(data[i] > 0) {
                $('td:eq('+i+')', row).addClass('bg-success');
              }
            }
          }
          /*"aoColumns": [
            {"sTitle": "Kode"},
            {"sTitle": "OPD"}
          ]*/
        });
    });
</script>
