<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/master/opd-index')?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        if ($this->input->get('error') == 1) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
          </div>
          <?php
        }
        if (validation_errors()) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
          </div>
          <?php
        }
        if (!empty($upload_errors)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$upload_errors?></span>
          </div>
          <?php
        }
        ?>
        <div class="card card-primary">
          <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group row">
                  <label class="control-label col-sm-3">Nama OPD</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Nama OPD" name="<?=COL_NM_SUB_UNIT?>" value="<?= $edit ? $data[COL_SKPDNAMA] : ""?>" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">Klasifikasi OPD</label>
                  <div class="col-sm-2">
                    <input type="number" class="form-control" placeholder="Urusan" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_SKPDURUSAN] : ""?>" required />
                  </div>
                  <div class="col-sm-2">
                    <input type="number" class="form-control" placeholder="Bidang" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_SKPDBIDANG] : ""?>" required />
                  </div>
                  <div class="col-sm-2">
                    <input type="number" class="form-control" placeholder="Unit" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_SKPDUNIT] : ""?>" required />
                  </div>
                  <div class="col-sm-2">
                    <input type="number" class="form-control" placeholder="Sub Unit" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_SKPDSUBUNIT] : ""?>" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-3">Pimpinan OPD</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Nama Pimpinan" name="<?=COL_NM_PIMPINAN?>" value="<?= $edit ? $data[COL_SKPDNAMAPIMPINAN] : ""?>" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row" style="text-align: center">
              <div class="col-md-12">
                <a href="<?=site_url('site/master/opd-index')?>" class="btn btn-default">KEMBALI</a>
                <button type="submit" class="btn btn-primary">SIMPAN</button>
              </div>
            </div>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
