<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_SKPDID] . '" />',
        anchor('site/master/opd-edit/'.$d[COL_SKPDID], $d[COL_SKPDNAMA]),
        $d[COL_SKPDISAKTIF]==1?'AKTIF':'SUSPEND',
        $d[COL_SKPDURUSAN].'.'.$d[COL_SKPDBIDANG].'.'.$d[COL_SKPDUNIT].'.'.$d[COL_SKPDSUBUNIT],
        $d[COL_SKPDNAMAPIMPINAN]
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small>Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <p>
                    <?=anchor('site/master/opd-delete', '<i class="far fa-trash"></i> Hapus', array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('site/master/opd-activate','<i class="far fa-check"></i> Aktifkan',array('class'=>'cekboxaction btn btn-success btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('site/master/opd-activate/0','<i class="far fa-warning"></i> Suspend',array('class'=>'cekboxaction btn btn-warning btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('site/master/opd-add', '<i class="far fa-plus"></i> Data Baru', array('class'=>'btn btn-primary btn-sm'))?>
                </p>
                <div class="card card-default">
                    <div class="card-body">
                        <form id="dataform" method="post" action="#">
                            <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
              "autoWidth" : false,
              //"sDom": "Rlfrtip",
              "aaData": <?=$data?>,
              //"bJQueryUI": true,
              //"aaSorting" : [[5,'desc']],
              "scrollY" : '40vh',
              "scrollX": "200%",
              "iDisplayLength": 100,
              "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
              "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
              "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
              "order": [[ 1, "asc" ]],
              "aoColumns": [
                  {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
                  {"sTitle": "Nama OPD"},
                  {"sTitle": "Status","bSortable":false},
                  {"sTitle": "Kode"},
                  {"sTitle": "Pimpinan"}
              ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>
