<?php
class Faq extends MY_Controller {
  function index() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    $loginuser = GetLoggedUser();
    if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
      show_error('Anda tidak memiliki akses terhadap modul ini.');
      return;
    }

    $data['title'] = "Keluhan / Aduan";
    $data['res'] = $this->db
    ->order_by(COL_TIMESTAMP, 'desc')
    ->get(TBL__FAQS)
    ->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('backend', 'faq/index', $data);
  }
}
