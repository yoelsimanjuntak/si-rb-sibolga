<?php
class Master extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!IsLogin()) {
            redirect('site/user/login');
        }
        if (GetLoggedUser()[COL_ROLEID]!=ROLEADMIN) {
            show_error('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }
    }

    public function opd_index()
    {
      $data['title'] = "OPD";
      $this->db->order_by(TBL_SAKIPV2_SKPD.".".COL_SKPDNAMA, 'asc');
      $data['res'] = $this->db->get(TBL_SAKIPV2_SKPD)->result_array();
      $this->template->load('backend', 'master/index_opd', $data);
    }

    public function opd_add()
    {
        $data['title'] = 'OPD';
        $data['edit'] = false;
        if (!empty($_POST)) {
            $data['data'] = $_POST;
            $rec = array(
              COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
              COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
              COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
              COL_KD_SUB=>$this->input->post(COL_KD_SUB),
              COL_NM_SUB_UNIT=>$this->input->post(COL_NM_SUB_UNIT)
            );
            $rec2 = array(
              COL_SKPDURUSAN=>$this->input->post(COL_KD_URUSAN),
              COL_SKPDBIDANG=>$this->input->post(COL_KD_BIDANG),
              COL_SKPDUNIT=>$this->input->post(COL_KD_UNIT),
              COL_SKPDSUBUNIT=>$this->input->post(COL_KD_SUB),
              COL_SKPDNAMA=>$this->input->post(COL_NM_SUB_UNIT),
              COL_SKPDNAMAPIMPINAN=>$this->input->post(COL_NM_PIMPINAN)
            );

            $this->db->trans_begin();
            try {
              $res = $this->db->insert(TBL_SAKIPV2_SKPD, $rec2);
              if(!$res) {
                throw new Exception("SERVER ERROR");
              }
              $res = $this->db->insert(TBL_AJBK_UNIT, $rec);
              if(!$res) {
                throw new Exception("SERVER ERROR");
              }
              $this->db->trans_commit();
              redirect('site/master/opd-index');
            } catch(Exception $ex) {
              $this->db->trans_rollback();
              $this->template->load('backend', 'master/form_opd', $data);
              return false;
            }
        } else {
            $this->template->load('backend', 'master/form_opd', $data);
        }
    }

    public function opd_edit($id)
    {
        $data['title'] = 'OPD';
        $data['edit'] = true;

        $data['data'] = $rdata = $this->db->where(COL_SKPDID, $id)->get(TBL_SAKIPV2_SKPD)->row_array();
        if (empty($rdata)) {
            show_404();
            return;
        }
        if (!empty($_POST)) {
            $data['data'] = $_POST;
            $rec = array(
              COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
              COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
              COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
              COL_KD_SUB=>$this->input->post(COL_KD_SUB),
              COL_NM_SUB_UNIT=>$this->input->post(COL_NM_SUB_UNIT)
            );
            $rec2 = array(
              COL_SKPDURUSAN=>$this->input->post(COL_KD_URUSAN),
              COL_SKPDBIDANG=>$this->input->post(COL_KD_BIDANG),
              COL_SKPDUNIT=>$this->input->post(COL_KD_UNIT),
              COL_SKPDSUBUNIT=>$this->input->post(COL_KD_SUB),
              COL_SKPDNAMA=>$this->input->post(COL_NM_SUB_UNIT),
              COL_SKPDNAMAPIMPINAN=>$this->input->post(COL_NM_PIMPINAN)
            );

            $this->db->trans_begin();
            try {
              $res = $this->db
              ->where(COL_SKPDID, $id)
              ->update(TBL_SAKIPV2_SKPD, $rec2);
              if(!$res) {
                throw new Exception("SERVER ERROR");
              }

              $res = $this->db
              ->where(array(
                COL_KD_URUSAN=>$rdata[COL_SKPDURUSAN],
                COL_KD_BIDANG=>$rdata[COL_SKPDBIDANG],
                COL_KD_UNIT=>$rdata[COL_SKPDUNIT],
                COL_KD_SUB=>$rdata[COL_SKPDSUBUNIT]
              ))
              ->update(TBL_AJBK_UNIT, $rec);
              if(!$res) {
                throw new Exception("SERVER ERROR");
              }
              $this->db->trans_commit();
              redirect('site/master/opd-index');
            } catch(Exception $ex) {
              $this->db->trans_rollback();
              $this->template->load('backend', 'master/form_opd', $data);
              return false;
            }
        } else {
            $this->template->load('backend', 'master/form_opd', $data);
        }
    }

    public function opd_delete()
    {
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $rdata = $this->db->where(COL_SKPDID, $datum)->get(TBL_SAKIPV2_SKPD)->row_array();
        if($rdata) {
          $this->db->delete(TBL_SAKIPV2_SKPD, array(COL_SKPDID => $datum));
          $this->db->delete(TBL_AJBK_UNIT, array(
            COL_KD_URUSAN=>$rdata[COL_SKPDURUSAN],
            COL_KD_BIDANG=>$rdata[COL_SKPDBIDANG],
            COL_KD_UNIT=>$rdata[COL_SKPDUNIT],
            COL_KD_SUB=>$rdata[COL_SKPDSUBUNIT]
          ));
          $deleted++;
        }

      }
      if ($deleted) {
        ShowJsonSuccess($deleted." data dihapus");
      } else {
        ShowJsonError("Tidak ada dihapus");
      }
    }

    function opd_activate($opt=1) {
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $rdata = $this->db->where(COL_SKPDID, $datum)->get(TBL_SAKIPV2_SKPD)->row_array();
        if($rdata) {
          $this->db->where(COL_SKPDID, $datum)->update(TBL_SAKIPV2_SKPD, array(COL_SKPDISAKTIF=>$opt));
          $this->db->where(array(
            COL_KD_URUSAN=>$rdata[COL_SKPDURUSAN],
            COL_KD_BIDANG=>$rdata[COL_SKPDBIDANG],
            COL_KD_UNIT=>$rdata[COL_SKPDUNIT],
            COL_KD_SUB=>$rdata[COL_SKPDSUBUNIT]
          ))->update(TBL_AJBK_UNIT, array(COL_ISAKTIF=>$opt));
          $deleted++;
        }
      }
      if($deleted){
        ShowJsonSuccess($deleted." data diubah");
      }else{
        ShowJsonError("Tidak ada data yang diubah");
      }
    }
}
