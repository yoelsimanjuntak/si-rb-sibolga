<?php
class Template {
		var $template_data = array();

		function set($content_area, $value)
		{
			$this->template_data[$content_area] = $value;
		}

		function load($template = '', $view = '' , $view_data = array(), $return = FALSE, $isExternalModule=false)
		{
			$this->CI =& get_instance();

			$this->set('content' , $this->CI->load->view($view, $view_data, TRUE));
			if($isExternalModule) {
				$this->CI->load->view($template, $this->template_data);
			} else {
				$this->CI->load->view('_layouts/'.$template, $this->template_data);
			}
		}
}
 ?>
