# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: sbg_rb
# Generation Time: 2024-06-06 14:31:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_faqs`;

CREATE TABLE `_faqs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NmNama` varchar(200) DEFAULT NULL,
  `NmKontak` varchar(200) DEFAULT NULL,
  `NmKeterangan` text,
  `Timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2023-08-14 15:32:19','http://localhost/sb-rb/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(2,'2023-08-14 15:32:19','http://localhost/sb-rb/rb/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(3,'2023-08-14 15:32:25','http://localhost/sb-rb/rb/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(4,'2023-08-14 15:32:25','http://localhost/sb-rb/rb/data/index/instansi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(5,'2023-08-14 15:32:34','http://localhost/sb-rb/rb/data/index/instansi/general.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(6,'2023-08-14 15:32:36','http://localhost/sb-rb/rb/data/index/instansi/tematik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(7,'2023-08-14 15:32:39','http://localhost/sb-rb/rb/data/index/unit/general.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(8,'2023-08-14 15:32:42','http://localhost/sb-rb/rb/data/index/unit/tematik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(9,'2023-08-14 15:33:47','http://localhost/sb-rb/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(10,'2023-08-14 15:33:47','http://localhost/sb-rb/rb/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(11,'2023-08-14 15:33:47','http://localhost/sb-rb/rb/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(12,'2023-08-14 15:33:47','http://localhost/sb-rb/rb/data/index/instansi.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(13,'2023-08-14 15:33:56','http://localhost/sb-rb/generate.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(14,'2023-08-14 15:33:58','http://localhost/sb-rb/generate/generated.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(15,'2023-08-14 15:34:23','http://localhost/sb-rb/rb/data/index/unit/tematik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(16,'2023-08-14 15:34:59','http://localhost/sb-rb/rb/data/index/unit/tematik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(17,'2023-08-14 15:35:00','http://localhost/sb-rb/rb/user/logout.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(18,'2023-08-14 15:35:00','http://localhost/sb-rb/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(19,'2023-08-14 15:35:01','http://localhost/sb-rb/rb/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(20,'2023-08-14 15:35:58','http://localhost/sb-rb/rb/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(21,'2023-08-14 15:35:58','http://localhost/sb-rb/rb/data/index/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'),
	(22,'2023-08-14 15:36:07','http://localhost/sb-rb/rb/data/index/unit/tematik.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Bappeda'),
	(3,'Operator OPD'),
	(4,'Operator Bidang OPD'),
	(5,'Operator Sub Bidang OPD'),
	(6,'Operator Keuangan'),
	(7,'Operator APIP'),
	(8,'Admin Kuisioner'),
	(99,'Guest');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SI REFORMASI BIROKRASI'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Reformasi Birokrasi'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','-'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','PEMERINTAH KOTA SIBOLGA'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Preloader_1.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(11) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(11) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(11) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	('bappeda','bappeda@esakip.sibolgakota.go.id','4.3.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('bkd','bkd@esakip.sibolgakota.go.id','4.5.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('bpbd','bpbd@esakip.sibolgakota.go.id','2.2.2.5','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('bpkpad','bpkpad@esakip.sibolgakota.go.id','4.4.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dinkes','dinkes@esakip.sibolgakota.go.id','1.2.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dinsos','dinsos@esakip.sibolgakota.go.id','1.6.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('disdikbud','disdikbud@esakip.sibolgakota.go.id','1.1.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dishub','dishub@esakip.sibolgakota.go.id','2.9.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('diskominfo','diskominfo@esakip.sibolgakota.go.id','2.10.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('disperindag','disperindag@esakip.sibolgakota.go.id','2.1.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('disperkim','disperkim@esakip.sibolgakota.go.id','1.1.1.3','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('disporapar','disporapar@esakip.sibolgakota.go.id','2.13.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dp2kb','dp2kb@esakip.sibolgakota.go.id','1.1.1.6','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dpmkp3a','dpmkp3a@esakip.sibolgakota.go.id','1.1.1.5','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dpmptsp','dpmptsp@esakip.sibolgakota.go.id','2.12.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('dukcapil','dukcapil@esakip.sibolgakota.go.id','2.6.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('itko','itko@esakip.sibolgakota.go.id','4.2.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('kesbangpol','kesbangpol@esakip.sibolgakota.go.id','2.2.2.3','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('ketapang','ketapang@esakip.sibolgakota.go.id','2.3.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('kopumkmnaker','kopumkmnaker@esakip.sibolgakota.go.id','2.11.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('perpustakaan','perpustakaan@esakip.sibolgakota.go.id','2.17.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('pupr','pupr@esakip.sibolgakota.go.id','1.3.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('rsud','rsud@esakip.sibolgakota.go.id','1.2.1.2','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('satpolpp','satpolpp@esakip.sibolgakota.go.id','2.2.2.4','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('sbg.kota','sbg.kota@esakip.sibolgakota.go.id','4.1.1.3','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('sbg.sambas','sbg.sambas@esakip.sibolgakota.go.id','4.1.1.6','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('sbg.selatan','sbg.selatan@esakip.sibolgakota.go.id','4.1.1.5','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('sbg.utara','sbg.utara@esakip.sibolgakota.go.id','4.1.1.4','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('setda','setda@esakip.sibolgakota.go.id','4.1.1.1','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('setwan','setwan@esakip.sibolgakota.go.id','4.1.1.2','Operator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2023-08-14 15:32:24','::1'),
	('bappeda','1c89525122289cf7890946d51b61c5c3',3,0,'2023-08-14 15:35:57','::1'),
	('bkd','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('bpbd','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('bpkpad','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dinkes','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dinsos','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('disdikbud','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dishub','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('diskominfo','1c89525122289cf7890946d51b61c5c3',3,0,'2022-09-29 20:55:24','180.241.47.49'),
	('disperindag','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('disperkim','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('disporapar','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dp2kb','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dpmkp3a','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dpmptsp','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('dukcapil','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('itko','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('kesbangpol','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('ketapang','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('kopumkmnaker','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('perpustakaan','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('pupr','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('rsud','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('satpolpp','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('sbg.kota','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('sbg.sambas','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('sbg.selatan','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('sbg.utara','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL),
	('setda','1c89525122289cf7890946d51b61c5c3',3,0,'2022-09-29 20:53:17','180.241.47.49'),
	('setwan','1c89525122289cf7890946d51b61c5c3',3,0,NULL,NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rb_doc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_doc`;

CREATE TABLE `rb_doc` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `MonevPeriod` int(11) NOT NULL,
  `DocName` varchar(200) DEFAULT NULL,
  `DocURL` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_mperubahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_mperubahan`;

CREATE TABLE `rb_mperubahan` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NmKategori` enum('GENERAL','TEMATIK') NOT NULL DEFAULT 'GENERAL',
  `NmPerubahan` varchar(50) DEFAULT NULL,
  `NmIndikator` text,
  `NmKegiatan` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `rb_mperubahan` WRITE;
/*!40000 ALTER TABLE `rb_mperubahan` DISABLE KEYS */;

INSERT INTO `rb_mperubahan` (`Uniq`, `NmKategori`, `NmPerubahan`, `NmIndikator`, `NmKegiatan`)
VALUES
	(1,'GENERAL','MANAJEMEN PERUBAHAN','Nilai Reformasi Birokrasi','Pengembangan dan Penguatan Nilai-Nilai untuk meningkatkan komitmen dan implementasi perubahan (reform);Penguatan nilai integritas;Pengembangan dan penguatan peran agen perubahan dan role model;Pengembangan budaya kerja dan cara kerja yang adaptif'),
	(2,'GENERAL','DEREGULASI KEBIJAKAN','Indeks Reformasi Hukum,Indeks Kualitas Kebijakan','Melakukan identifikasi dan pemetaan regulasi lingkup IP (menghilangkan overlapping peraturan);Deregulasi aturan yang menghambat birokrasi;Penguatan Sistem Regulasi Nasional di lingkup IP;Melakukan perencanaan kebijakan yang meliputi agenda setting dan formulasi kebijakan;Melakukan evaluasi kemanfaatan kebijakan yang telah disusun'),
	(3,'GENERAL','PENATAAN ORGANISASI / KELEMBAGAAN','Indeks Kelembagaan','Assesment organisasi berbasis kinerja;Restrukturisasi (penyederhanaan) kelembagaan IP berdasarkan hasil assesment.;Membentuk struktur organisasi yang tepat fungsi'),
	(4,'GENERAL','PENATAAN TATA LAKSANA','Indeks SPBE,Indeks Pengawasan Kearsipan,Indeks Pengelolaan Keuangan,Indeks Pengelolaan Aset','Penerapan Tata Kelola SPBE;Penerapan Manajemen SPBE;Penerapan Layanan SBPE;Mengintegrasikan pemanfaatan IT dalam tata kelola pemerintahan;Implementasi manajemen kearsipan modern dan handal (dari manual ke digital);Melakukan pengelolaan arsip sesuai aturan;Mengimplementasikan digitalisasi arsip'),
	(5,'GENERAL','SISTEM MANAJEMEN SDM','Indeks Profesionalitas ASN,Indeks Sistem Merit','Menerapkan prinsip-prinsip manajemen ASN secara professional;Implementasi manajemen ASN berbasis merit system;Penetapan ukuran kinerja individu;Monitoring dan evaluasi kinerja individu secara berkala;Penguatan implementasi Reward and Punishment berdasarkan kinerja;Pengembangan kompetensi dan karir ASN berdasarkan hasil monitoring dan evaluasi kinerja dan kebutuhan organisasi;Pemanfaatan IT dalam manajemen ASN;Pengembangan nilai-nilai untuk menegakkan integritas ASN;Pengembangan implementasi Manajemen Talenta (talent pool);Penguatan database dan sistem informasi kepegawaian untuk pengembangan karir dan talenta ASN'),
	(6,'GENERAL','PENGUATAN AKUNTABILITAS','Nilai SAKIP,Indeks Perencanaan Pembangunan','Melakukan perencanaan terintegrasi dan perencanaan yang lintas sektor (collaborative and crosscutting);Penguatan keterlibatan pimpinan dan seluruh penanggungjawab dalam perencanaan kinerja, monitoring dan evaluasi kinerja, serta pelaporan kinerja;Peningkatan kualitas penyelarasan kinerja unit kepada kinerja organisasi (goal and strategy cascade);Pelaksanaan monitoring dan evaluasi kinerja secara berkala;Pengembangan dan pengintegrasian sistem informasi kinerja, perencanaan dan penganggaran;Penguatan implementasi value for money dalam rangka merealisasikan anggaran berbasis kinerja'),
	(7,'GENERAL','PENGAWASAN','Maturitas SPIP,Kapabilitas APIP,Opini BPK,Indeks Tata Kelola Pengadaan Barang dan Jasa,Indeks Persepsi Anti Korupsi','Melakukan penguatan implementasi SPIP di seluruh bagian organisasi;Meningkatkan kompetensi APIP;Pemenuhan rasio APIP (pemenuhan jumlah ideal aparatur pengawas);Melakukan pengelolaan dan dan akuntabilitas keuangan sesuai kaedah dan aturan yang berlaku;Melakukan pengelolaan barang dan jasa sesuai aturan;Pembangunan unit kerja Zona Integritas Menuju WBK/WBBM;Penguatan pengendalian gratifikasi;Penguatan penanganan pengaduan dan komplain;Penguatan efektifitas manajemen resiko;Pelaksanaan pemantauan benturan kepentingan'),
	(8,'GENERAL','PELAYANAN PUBLIK','Indeks Pelayanan Publik,Tingkat Kepatuhan terhadap Standar Pelayanan,Indeks Kepuasan Masyarakat','Melakukan penguatan implementasi kebijakan bidang pelayanan publik (Standar Pelayanan, Maklumat Pelayanan, SKM);Pengembangan dan pengintegrasian sistem informasi pelayanan publik dalam rangka peningkatan akses publik dalam rangka memperoleh informasi pelayanan;Pengelolaan pengaduan pelayanan publik secara terpadu, tuntas dan berkelanjutan dalam rangka memberikan akses kepada publik dalam rangka mendapatkan pelayanan yang baik;Peningkatan pelayanan publik berbasis elektronik dalam rangka memberikan pelayanan yang mudah, murah, cepat dan terjangkau;Penciptaan, pengembangan dan pelembagaan inovasi pelayanan publik dalam rangka percepatan peningkatan kualitas pelayanan publik;Pengembangan sistem pelayanan dengan mengintegrasikan pelayanan pusat, daerah dan bisnis dalam Mal Pelayanan Publik;Pengukuran kepuasan masyarakat secara berkala;Pelaksanaan monitoring dan evaluasi pelaksanaan kebijakan pelayanan publik secara berkala;Mendorong perangkat daerah dan unit kerja untuk melaksanakan survei kepuasan masyarakat;Meningkatkan tindak lanjut dari hasil survei kepuasan masyarakat'),
	(9,'TEMATIK','PENGENTASAN KEMISKINAN',NULL,NULL),
	(10,'TEMATIK','PENINGKATAN INVESTASI',NULL,NULL),
	(11,'TEMATIK','DIGITALISASI ADMINISTRASI PEMERINTAHAN',NULL,NULL),
	(12,'TEMATIK','PENINGKATAN PENGGUNAAN PRODUK DALAM NEGERI',NULL,NULL),
	(13,'TEMATIK','PENGENDALIAN INFLASI',NULL,NULL),
	(14,'TEMATIK','PENANGANAN STUNTING',NULL,NULL);

/*!40000 ALTER TABLE `rb_mperubahan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rb_renja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renja`;

CREATE TABLE `rb_renja` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Tahun` int(11) NOT NULL,
  `NmType` enum('INSTANSI','UNIT') NOT NULL DEFAULT 'INSTANSI',
  `NmKategori` enum('GENERAL','TEMATIK') NOT NULL DEFAULT 'GENERAL',
  `NmKeterangan` text,
  `SkpdId` bigint(20) DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renjadet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renjadet`;

CREATE TABLE `rb_renjadet` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `IdPerubahan` bigint(20) unsigned NOT NULL,
  `NmKegiatan` text,
  `NmTahapan` text,
  `NmTarget` varchar(100) DEFAULT NULL,
  `NmSatuan` varchar(100) DEFAULT NULL,
  `NmOutput` text,
  `NmIndikator` text,
  `NmSubKegiatan` text,
  `NmPenanggungJawab` text,
  `PeriodTarget` text,
  PRIMARY KEY (`Uniq`),
  KEY `FK_RENJADET_RENJA` (`IdRenja`),
  KEY `FK_RENJADET_INDIKATOR` (`IdPerubahan`),
  CONSTRAINT `FK_RENJADET_INDIKATOR` FOREIGN KEY (`IdPerubahan`) REFERENCES `rb_renjaperubahan` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RENJADET_RENJA` FOREIGN KEY (`IdRenja`) REFERENCES `rb_renja` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renjamonev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renjamonev`;

CREATE TABLE `rb_renjamonev` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `IdTahapan` bigint(20) unsigned NOT NULL,
  `MonevPeriod` int(11) NOT NULL,
  `MonevKeterangan` text,
  `MonevTarget` varchar(100) DEFAULT NULL,
  `MonevCapaian` varchar(100) NOT NULL,
  `MonevEvidence` text,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_RENJAMONEV_RENJA` (`IdRenja`),
  KEY `FK_RENJAMONEV_TAHAPAN` (`IdTahapan`),
  CONSTRAINT `FK_RENJAMONEV_RENJA` FOREIGN KEY (`IdRenja`) REFERENCES `rb_renja` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RENJAMONEV_TAHAPAN` FOREIGN KEY (`IdTahapan`) REFERENCES `rb_renjadet` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table rb_renjaperubahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rb_renjaperubahan`;

CREATE TABLE `rb_renjaperubahan` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `IdRenja` bigint(20) unsigned NOT NULL,
  `IdSkpd` bigint(20) DEFAULT NULL,
  `NmPerubahan` text,
  `NmKategori` enum('GENERAL','TEMATIK') NOT NULL DEFAULT 'GENERAL',
  `NmIndikator` text,
  `NmKegiatan` text,
  `NmOutput` text,
  `NmTarget` varchar(100) DEFAULT NULL,
  `NmPenanggungJawab` text,
  `NmKriteria` text,
  PRIMARY KEY (`Uniq`),
  KEY `FK_RENJAKEG_RENJA` (`IdRenja`),
  CONSTRAINT `FK_RENJAKEG_RENJA` FOREIGN KEY (`IdRenja`) REFERENCES `rb_renja` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ref_sub_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_sub_unit`;

CREATE TABLE `ref_sub_unit` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(20) NOT NULL,
  `Kd_Bidang` bigint(20) NOT NULL,
  `Kd_Unit` bigint(20) NOT NULL,
  `Kd_Sub` bigint(20) NOT NULL,
  `Nm_Sub_Unit` varchar(255) NOT NULL,
  `Nm_Pimpinan` varchar(255) DEFAULT NULL,
  `Nm_Kop1` varchar(255) DEFAULT NULL,
  `Nm_Kop2` varchar(255) DEFAULT NULL,
  `IsAktif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`) USING BTREE,
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sakipv2_bid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid`;

CREATE TABLE `sakipv2_bid` (
  `IdRenstra` bigint(20) unsigned NOT NULL,
  `BidId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `BidNama` varchar(200) NOT NULL DEFAULT '',
  `BidNamaPimpinan` varchar(200) DEFAULT NULL,
  `BidNamaJabatan` varchar(200) DEFAULT NULL,
  `BidIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `BidTugasPokok` text,
  `BidFungsi` text,
  `BidIKU` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`BidId`),
  KEY `FK_BID_RENSTRA` (`IdRenstra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_kegiatan`;

CREATE TABLE `sakipv2_bid_kegiatan` (
  `IdProgram` bigint(20) unsigned NOT NULL,
  `KegiatanId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `KegiatanKode` varchar(200) DEFAULT NULL,
  `KegiatanUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`KegiatanId`),
  KEY `FK_KEGIATAN_PROGRAM` (`IdProgram`),
  CONSTRAINT `FK_KEGIATAN_PROGRAM` FOREIGN KEY (`IdProgram`) REFERENCES `sakipv2_bid_program` (`ProgramId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_kegsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_kegsasaran`;

CREATE TABLE `sakipv2_bid_kegsasaran` (
  `IdKegiatan` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) DEFAULT NULL,
  `SasaranUraian` text,
  `SasaranIndikator` text,
  `SasaranSatuan` text,
  `SasaranTarget` text,
  `SasaranTargetTW1` text,
  `SasaranTargetTW2` text,
  `SasaranTargetTW3` text,
  `SasaranTargetTW4` text,
  `SasaranRealisasi` text,
  `SasaranRealisasiTW1` text,
  `SasaranRealisasiTW2` text,
  `SasaranRealisasiTW3` text,
  `SasaranRealisasiTW4` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_KEGSASARAN_KEGIATAN` (`IdKegiatan`),
  CONSTRAINT `FK_KEGSASARAN_KEGIATAN` FOREIGN KEY (`IdKegiatan`) REFERENCES `sakipv2_bid_kegiatan` (`KegiatanId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_program`;

CREATE TABLE `sakipv2_bid_program` (
  `IdBid` bigint(20) unsigned NOT NULL,
  `IdDPA` bigint(20) unsigned NOT NULL,
  `IdSasaranSkpd` bigint(20) unsigned NOT NULL,
  `ProgramId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ProgramKode` varchar(200) DEFAULT NULL,
  `ProgramUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ProgramId`),
  KEY `FK_PROGRAM_BID` (`IdBid`),
  KEY `FK_PROGRAM_DPA` (`IdDPA`),
  KEY `FK_PROGRAM_PMDSASARAN` (`IdSasaranSkpd`),
  CONSTRAINT `FK_PROGRAM_BID` FOREIGN KEY (`IdBid`) REFERENCES `sakipv2_bid` (`BidId`),
  CONSTRAINT `FK_PROGRAM_DPA` FOREIGN KEY (`IdDPA`) REFERENCES `sakipv2_skpd_renstra_dpa` (`DPAId`),
  CONSTRAINT `FK_PROGRAM_RENSTRASASARAN` FOREIGN KEY (`IdSasaranSkpd`) REFERENCES `sakipv2_skpd_renstra_sasaran` (`SasaranId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_bid_progsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_bid_progsasaran`;

CREATE TABLE `sakipv2_bid_progsasaran` (
  `IdProgram` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) DEFAULT NULL,
  `SasaranUraian` text,
  `SasaranIndikator` text,
  `SasaranSatuan` text,
  `SasaranTarget` text,
  `SasaranTargetTW1` text,
  `SasaranTargetTW2` text,
  `SasaranTargetTW3` text,
  `SasaranTargetTW4` text,
  `SasaranRealisasi` text,
  `SasaranRealisasiTW1` text,
  `SasaranRealisasiTW2` text,
  `SasaranRealisasiTW3` text,
  `SasaranRealisasiTW4` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_PROGSASARAN_PROG` (`IdProgram`),
  CONSTRAINT `FK_PROGSASARAN_PROG` FOREIGN KEY (`IdProgram`) REFERENCES `sakipv2_bid_program` (`ProgramId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda`;

CREATE TABLE `sakipv2_pemda` (
  `PmdId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PmdTahunMulai` int(11) NOT NULL,
  `PmdTahunAkhir` int(11) NOT NULL,
  `PmdPejabat` varchar(200) DEFAULT NULL,
  `PmdPejabatWakil` varchar(200) DEFAULT NULL,
  `PmdVisi` text,
  `PmdIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `PmdIsPenjabat` tinyint(1) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PmdId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_misi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_misi`;

CREATE TABLE `sakipv2_pemda_misi` (
  `IdPmd` bigint(20) unsigned NOT NULL,
  `MisiId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MisiNo` int(11) NOT NULL,
  `MisiUraian` text NOT NULL,
  `MisiIKU` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`MisiId`),
  KEY `FK_MISI_PMD` (`IdPmd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_sasaran`;

CREATE TABLE `sakipv2_pemda_sasaran` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) NOT NULL,
  `SasaranUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_SASARAN_TUJUAN` (`IdTujuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_sasarandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_sasarandet`;

CREATE TABLE `sakipv2_pemda_sasarandet` (
  `IdSasaran` bigint(20) unsigned NOT NULL,
  `SsrIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SsrIndikatorUraian` text,
  `SsrIndikatorSumberData` text,
  `SsrIndikatorFormulasi` text,
  `SsrIndikatorSatuan` text,
  `SsrIndikatorTarget` text,
  `SsrIndikatorRealisasi` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SsrIndikatorId`),
  KEY `FK_SASARANDET_SASARAN` (`IdSasaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_sasaranmonev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_sasaranmonev`;

CREATE TABLE `sakipv2_pemda_sasaranmonev` (
  `IdSasaranIndikator` bigint(20) unsigned DEFAULT NULL,
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MonevTahun` int(11) NOT NULL,
  `MonevTarget` text,
  `MonevTargetTW1` text,
  `MonevTargetTW2` text,
  `MonevTargetTW3` text,
  `MonevTargetTW4` text,
  `MonevRealisasi` text,
  `MonevRealisasiTW1` text,
  `MonevRealisasiTW2` text,
  `MonevRealisasiTW3` text,
  `MonevRealisasiTW4` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_MONEV_PMDSASARANDET` (`IdSasaranIndikator`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_tujuan`;

CREATE TABLE `sakipv2_pemda_tujuan` (
  `IdMisi` bigint(20) unsigned NOT NULL,
  `TujuanId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujuanNo` int(11) NOT NULL,
  `TujuanUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujuanId`),
  KEY `FK_TUJUAN_MISI` (`IdMisi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_pemda_tujuandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_pemda_tujuandet`;

CREATE TABLE `sakipv2_pemda_tujuandet` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `TujIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujIndikatorUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujIndikatorId`),
  KEY `FK_TUJUANDET_TUJUAN` (`IdTujuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd`;

CREATE TABLE `sakipv2_skpd` (
  `SkpdId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SkpdNama` text,
  `SkpdNamaPimpinan` varchar(200) DEFAULT NULL,
  `SkpdNamaJabatan` varchar(200) DEFAULT NULL,
  `SkpdUrusan` bigint(20) NOT NULL,
  `SkpdBidang` bigint(20) NOT NULL,
  `SkpdUnit` bigint(20) NOT NULL,
  `SkpdSubUnit` bigint(20) NOT NULL,
  `SkpdKop` text,
  `SkpdIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SkpdId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakipv2_skpd` WRITE;
/*!40000 ALTER TABLE `sakipv2_skpd` DISABLE KEYS */;

INSERT INTO `sakipv2_skpd` (`SkpdId`, `SkpdNama`, `SkpdNamaPimpinan`, `SkpdNamaJabatan`, `SkpdUrusan`, `SkpdBidang`, `SkpdUnit`, `SkpdSubUnit`, `SkpdKop`, `SkpdIsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'SEKRETARIAT DAERAH',NULL,NULL,4,1,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(2,'SEKRETARIAT DPRD',NULL,NULL,4,1,1,2,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(3,'INSPEKTORAT',NULL,NULL,4,2,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(4,'DINAS PENDIDIKAN DAN KEBUDAYAAN',NULL,NULL,1,1,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(5,'DINAS KESEHATAN',NULL,NULL,1,2,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(6,'DINAS PEKERJAAN UMUM DAN PENATAAN RUANG',NULL,NULL,1,3,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(7,'DINAS PERUMAHAN, KAWASAN, PERMUKIMAN DAN LINGKUNGAN HIDUP',NULL,NULL,1,1,1,3,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(8,'DINAS SOSIAL',NULL,NULL,1,6,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(9,'DINAS KOPERASI, USAHA KECIL DAN MENENGAH DAN KETENAGAKERJAAN',NULL,NULL,2,11,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(10,'DINAS PENGENDALIAN PENDUDUK DAN KELUARGA BERENCANA',NULL,NULL,1,1,1,6,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(11,'DINAS PERIKANAN, KETAHANAN PANGAN DAN PERTANIAN',NULL,NULL,2,3,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(12,'DINAS KEPENDUDUKAN DAN PENCACATAN SIPIL',NULL,NULL,2,6,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(13,'DINAS PEMBERDAYAAN MASYARAKAT KELURAHAN, PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK',NULL,NULL,1,1,1,5,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(14,'DINAS PERHUBUNGAN',NULL,NULL,2,9,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(15,'DINAS KOMUNIKASI DAN INFORMASI',NULL,NULL,2,10,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(16,'DINAS PENANAMAN MODAL DAN PELAYANAN PERIZINAN TERPADU SATU PINTU',NULL,NULL,2,12,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(17,'DINAS PARIWISATA, PEMUDA DAN OLAHRAGA',NULL,NULL,2,13,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(18,'DINAS PERINDUSTRIAN DAN PERDAGANGAN',NULL,NULL,2,1,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(19,'DINAS PERPUSTAKAAN',NULL,NULL,2,17,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(20,'SATUAN POLISI PAMONG PRAJA',NULL,NULL,2,2,2,4,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(21,'BADAN PERENCANAAN PEMBANGUNAN DAERAH',NULL,NULL,4,3,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(22,'BADAN PENGELOLAAN KEUANGAN, PENDAPATAN DAN ASET DAERAH',NULL,NULL,4,4,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(23,'BADAN KEPEGAWAIAN DAERAH',NULL,NULL,4,5,1,1,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(24,'KECAMATAN SIBOLGA UTARA',NULL,NULL,4,1,1,4,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(25,'KECAMATAN SIBOLGA KOTA',NULL,NULL,4,1,1,3,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(26,'KECAMATAN SIBOLGA SAMBAS',NULL,NULL,4,1,1,6,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(27,'KECAMATAN SIBOLGA SELATAN',NULL,NULL,4,1,1,5,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(28,'BADAN PENANGGULANGAN BENCANA DAERAH',NULL,NULL,2,2,2,5,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(29,'KANTOR KESATUAN BANGSA DAN POLITIK',NULL,NULL,2,2,2,3,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL),
	(30,'RUMAH SAKIT UMUM F.L TOBING',NULL,NULL,1,2,1,2,NULL,1,'admin','2022-07-06 22:33:44',NULL,NULL);

/*!40000 ALTER TABLE `sakipv2_skpd` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakipv2_skpd_doc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_doc`;

CREATE TABLE `sakipv2_skpd_doc` (
  `IdSkpd` bigint(20) unsigned DEFAULT NULL,
  `DocId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DocName` text NOT NULL,
  `DocRemarks` text,
  `DocTahun` int(11) DEFAULT NULL,
  `DocURL` text NOT NULL,
  `DocType` enum('renstra','pohon-kinerja-ip','pohon-kinerja','crosscutting','lakip','lainnya','lke-rb','ren-rb','rb-lainnya') DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`DocId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra`;

CREATE TABLE `sakipv2_skpd_renstra` (
  `IdSkpd` bigint(20) unsigned NOT NULL,
  `IdPemda` bigint(20) unsigned NOT NULL,
  `RenstraId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `RenstraTahun` int(11) NOT NULL,
  `RenstraUraian` text,
  `RenstraIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `RenstraTugasPokok` text,
  `RenstraFungsi` text,
  `RenstraIKU` text,
  `RenstraOrg` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`RenstraId`),
  KEY `FK_RENSTRA_SKPD` (`IdSkpd`),
  KEY `FK_RENSTRA_PMD` (`IdPemda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_dpa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_dpa`;

CREATE TABLE `sakipv2_skpd_renstra_dpa` (
  `IdRenstra` bigint(20) unsigned NOT NULL,
  `DPAId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DPATahun` int(11) NOT NULL,
  `DPAUraian` text,
  `DPAIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `LK11` text,
  `LK12` text,
  `LK13` text,
  `LK14` text,
  `LK21` text,
  `LK22` text,
  `LK23` text,
  `LK24` text,
  `LK31` text,
  `LK32` text,
  `LK4` text,
  PRIMARY KEY (`DPAId`),
  KEY `FK_DPARENSTRA_RENSTRA` (`IdRenstra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_sasaran`;

CREATE TABLE `sakipv2_skpd_renstra_sasaran` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `IdSasaranPmd` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) NOT NULL,
  `SasaranUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_RENSTRASASARAN_TUJUAN` (`IdTujuan`),
  KEY `FK_RENSTRASASARAN_PMDSASARAN` (`IdSasaranPmd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_sasarandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_sasarandet`;

CREATE TABLE `sakipv2_skpd_renstra_sasarandet` (
  `IdSasaran` bigint(20) unsigned NOT NULL,
  `SsrIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SsrIndikatorUraian` text,
  `SsrIndikatorSumberData` text,
  `SsrIndikatorFormulasi` text,
  `SsrIndikatorSatuan` text,
  `SsrIndikatorTarget` text,
  `SsrIndikatorRealisasi` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SsrIndikatorId`),
  KEY `FK_RENSTRASASARANDET_SASARAN` (`IdSasaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_sasaranmonev
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_sasaranmonev`;

CREATE TABLE `sakipv2_skpd_renstra_sasaranmonev` (
  `IdSasaranIndikator` bigint(20) unsigned DEFAULT NULL,
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MonevTahun` int(11) NOT NULL,
  `MonevTarget` text,
  `MonevTargetTW1` text,
  `MonevTargetTW2` text,
  `MonevTargetTW3` text,
  `MonevTargetTW4` text,
  `MonevRealisasi` text,
  `MonevRealisasiTW1` text,
  `MonevRealisasiTW2` text,
  `MonevRealisasiTW3` text,
  `MonevRealisasiTW4` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_MONEV_OPDSASARAN` (`IdSasaranIndikator`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_tujuan`;

CREATE TABLE `sakipv2_skpd_renstra_tujuan` (
  `IdRenstra` bigint(20) unsigned NOT NULL,
  `IdTujuanPmd` bigint(20) unsigned NOT NULL,
  `TujuanId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujuanNo` int(11) NOT NULL,
  `TujuanUraian` text NOT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujuanId`),
  KEY `FK_TUJUAN_RENSTRA` (`IdRenstra`),
  KEY `FK_RENSTRATUJUAN_PMDTUJUAN` (`IdTujuanPmd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_skpd_renstra_tujuandet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_skpd_renstra_tujuandet`;

CREATE TABLE `sakipv2_skpd_renstra_tujuandet` (
  `IdTujuan` bigint(20) unsigned NOT NULL,
  `TujIndikatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TujIndikatorUraian` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`TujIndikatorId`),
  KEY `FK_RENSTUJUANDET_TUJUAN` (`IdTujuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid`;

CREATE TABLE `sakipv2_subbid` (
  `IdBid` bigint(20) DEFAULT NULL,
  `SubbidId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SubbidNama` varchar(200) NOT NULL DEFAULT '',
  `SubbidNamaPimpinan` varchar(200) DEFAULT NULL,
  `SubbidNamaJabatan` varchar(200) DEFAULT NULL,
  `SubbidIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `SubbidTugasPokok` text,
  `SubbidFungsi` text,
  `SubbidIKU` text,
  `SubbidAtasan` varchar(200) DEFAULT NULL,
  `SubbidAtasanNama` varchar(200) DEFAULT NULL,
  `SubbidAtasanJab` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SubbidId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid_pelaksana
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid_pelaksana`;

CREATE TABLE `sakipv2_subbid_pelaksana` (
  `IdSubbid` bigint(20) NOT NULL,
  `PlsId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PlsNama` varchar(200) DEFAULT NULL,
  `PlsNamaPegawai` varchar(200) DEFAULT NULL,
  `PlsTugasPokok` text,
  `PlsFungsi` text,
  `PlsIKU` text,
  `PlsAtasan` varchar(200) DEFAULT NULL,
  `PlsAtasanNama` varchar(200) DEFAULT NULL,
  `PlsAtasanJab` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PlsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid_subkegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid_subkegiatan`;

CREATE TABLE `sakipv2_subbid_subkegiatan` (
  `IdSubbid` bigint(20) DEFAULT NULL,
  `IdKegiatan` bigint(20) unsigned NOT NULL,
  `SubkegId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SubkegKode` varchar(200) DEFAULT NULL,
  `SubkegUraian` text,
  `SubkegPagu` double DEFAULT NULL,
  `SubkegRealisasi` double DEFAULT NULL,
  `SubkegPaguTW1` double DEFAULT NULL,
  `SubkegPaguTW2` double DEFAULT NULL,
  `SubkegPaguTW3` double DEFAULT NULL,
  `SubkegPaguTW4` double DEFAULT NULL,
  `SubkegRealisasiTW1` double DEFAULT NULL,
  `SubkegRealisasiTW2` double DEFAULT NULL,
  `SubkegRealisasiTW3` double DEFAULT NULL,
  `SubkegRealisasiTW4` double DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SubkegId`),
  KEY `FK_SUBKEG_KEGIATAN` (`IdKegiatan`),
  CONSTRAINT `FK_SUBKEG_KEGIATAN` FOREIGN KEY (`IdKegiatan`) REFERENCES `sakipv2_bid_kegiatan` (`KegiatanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakipv2_subbid_subkegsasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakipv2_subbid_subkegsasaran`;

CREATE TABLE `sakipv2_subbid_subkegsasaran` (
  `IdSubkeg` bigint(20) unsigned NOT NULL,
  `SasaranId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SasaranNo` int(11) DEFAULT NULL,
  `SasaranUraian` text,
  `SasaranIndikator` text,
  `SasaranSatuan` text,
  `SasaranTarget` text,
  `SasaranTargetTW1` text,
  `SasaranTargetTW2` text,
  `SasaranTargetTW3` text,
  `SasaranTargetTW4` text,
  `SasaranRealisasi` text,
  `SasaranRealisasiTW1` text,
  `SasaranRealisasiTW2` text,
  `SasaranRealisasiTW3` text,
  `SasaranRealisasiTW4` text,
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SasaranId`),
  KEY `FK_SUBKEGSASARAN_SUBKEG` (`IdSubkeg`),
  CONSTRAINT `FK_SUBKEGSASARAN_SUBKEG` FOREIGN KEY (`IdSubkeg`) REFERENCES `sakipv2_subbid_subkegiatan` (`SubkegId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
